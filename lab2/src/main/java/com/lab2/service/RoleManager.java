package com.lab2.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.lab2.domain.Role;


public class RoleManager  {

	private Connection connection;

	private String url = "jdbc:hsqldb:hsql://localhost/workdb";
	
	private String createTableRole = "CREATE TABLE Role(id bigint GENERATED BY DEFAULT AS IDENTITY, role varchar(20))";
	
	private PreparedStatement addRoleStmt;
	private PreparedStatement deleteAllRolesStmt;
	private PreparedStatement getAllRolesStmt;
	
	private Statement statement;
	
	public RoleManager()
	{
		try {
			connection = DriverManager.getConnection(url);
			statement = connection.createStatement();

			ResultSet rs = connection.getMetaData().getTables(null, null, null,
					null);
			boolean tableExists = false;
			while (rs.next()) 
			{
				if ("Role".equalsIgnoreCase(rs.getString("TABLE_NAME"))) 
				{
					tableExists = true;
					break;
				}
			}
			if (!tableExists)
				statement.executeUpdate(createTableRole);

			addRoleStmt = connection
					.prepareStatement("INSERT INTO Role (role) VALUES (?)");
			deleteAllRolesStmt = connection
					.prepareStatement("DELETE FROM Role");
			getAllRolesStmt = connection
					.prepareStatement("SELECT id, role FROM Role");

		} catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
	
	Connection getConnection() {
		return connection;
	}

	void clearRoles() {
		try {
			deleteAllRolesStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public int addRole(Role r) {
		int count = 0;
		try {
			addRoleStmt.setString(1, r.getRole());

			count = addRoleStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

	public List<Role> getAllRoles() {
		List<Role> roles = new ArrayList<Role>();

		try {
			ResultSet rs = getAllRolesStmt.executeQuery();

			while (rs.next()) {
				Role r = new Role();
				r.setId(rs.getInt("id"));
				r.setRole(rs.getString("role"));
				roles.add(r);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return roles;
	}

	
	
}

