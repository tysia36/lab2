/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lab2.domain;

/**
 *
 * @author j.pluzinska
 */
public class Address {
    
	private long id;
    private String street;
    private int houseNumber;
    private int flatNumber;
    private String city;
    
    public Address() {
	}
	
	public Address(String street, int house, int flat, String city) {
		super();
		this.street = street;
		this.houseNumber = house;
		this.flatNumber = flat;
		this.city = city;
	}
    
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
    
    public String getStreet() 
    {
	return this.street;
    }

    public void setStreet(String street) 
    {
	this.street = street;
    }
    public String getCity() 
    {
	return this.city;
    }

    public void setCity(String city) 
    {
	this.city = city;
    }
    
    public int getHouseNumber() 
    {
	return this.houseNumber;
    }
    
    public int getFlatNumber() 
    {
	return this.flatNumber;
    }
    
    public void setHouseNumber(int nr) 
    {
	this.houseNumber = nr;
    }
    
    public void setFlatNumber(int nr) 
    {
	this.flatNumber = nr;
    }
}
