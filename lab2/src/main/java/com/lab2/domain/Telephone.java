/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lab2.domain;

/**
 *
 * @author j.pluzinska
 */
public class Telephone {
    
	private long id;
    private int tel;
	
    public Telephone() {
 	}
 	
 	public Telephone(int tel) {
 		super();
 		this.tel=tel;
 	}
     
     public long getId() {
 		return id;
 	}

 	public void setId(long id) {
 		this.id = id;
 	}

	public int getTel() {
		return tel;
	}

	public void setTel(int tel) {
		this.tel = tel;
	}
	
        
}

