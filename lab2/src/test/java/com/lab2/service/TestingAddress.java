package com.lab2.service;


import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.lab2.domain.Address;
import com.lab2.service.AddressManager;

public class TestingAddress {
		
		
		AddressManager addressManager = new AddressManager();
		
		private final static String STREET_1 = "Zielna";
		private final static int HOUSE_1 = 3;
		private final static int FLAT_1 = 7;
		private final static String CITY_1 = "Zielona Podkowa";
		
		@Test
		public void checkConnection(){
			assertNotNull(addressManager.getConnection());
		}
		
		@Test
		public void checkAdding(){
			
			Address address = new Address(STREET_1, HOUSE_1, FLAT_1, CITY_1);
			
			addressManager.clearAddresses();
			assertEquals(1,addressManager.addAddress(address));
			
			List<Address> addresses = addressManager.getAllAddresses();
			Address addressRetrieved = addresses.get(0);
			
			assertEquals(STREET_1, addressRetrieved.getStreet());
			assertEquals(HOUSE_1, addressRetrieved.getHouseNumber());
			assertEquals(FLAT_1, addressRetrieved.getFlatNumber());
			assertEquals(CITY_1, addressRetrieved.getCity());
			
		}

	

}
