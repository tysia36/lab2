package com.lab2.service;


import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.lab2.domain.Person;
import com.lab2.service.PersonManager;

public class TestingPerson {
		
		
		PersonManager personManager = new PersonManager();
		
		private final static String LOGIN_1 = "Renoz";
		private final static String SURNAME_1 = "Tajne";
		
		@Test
		public void checkConnection(){
			assertNotNull(personManager.getConnection());
		}
		
		@Test
		public void checkAdding(){
			
			Person person = new Person(LOGIN_1, SURNAME_1);
			
			personManager.clearPersons();
			assertEquals(1,personManager.addPerson(person));
			
			List<Person> persons = personManager.getAllPersons();
			Person personRetrieved = persons.get(0);
			
			assertEquals(LOGIN_1, personRetrieved.getName());
			assertEquals(SURNAME_1, personRetrieved.getSurname());
			
		}

	

}
