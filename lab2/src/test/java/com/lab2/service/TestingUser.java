package com.lab2.service;


import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.lab2.domain.User;
import com.lab2.service.UserManager;

public class TestingUser {
		
		
		UserManager userManager = new UserManager();
		
		private final static String LOGIN_1 = "Renoz";
		private final static String PASSWORD_1 = "Tajne";
		
		@Test
		public void checkConnection(){
			assertNotNull(userManager.getConnection());
		}
		
		@Test
		public void checkAdding(){
			
			User user = new User(LOGIN_1, PASSWORD_1);
			
			userManager.clearUsers();
			assertEquals(1,userManager.addUser(user));
			
			List<User> users = userManager.getAllUsers();
			User userRetrieved = users.get(0);
			
			assertEquals(LOGIN_1, userRetrieved.getLogin());
			assertEquals(PASSWORD_1, userRetrieved.getPassword());
			
		}

	

}
