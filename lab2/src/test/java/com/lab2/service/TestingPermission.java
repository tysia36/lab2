package com.lab2.service;


import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.lab2.domain.Permission;
import com.lab2.service.PermissionManager;

public class TestingPermission {
		
		
		PermissionManager permissionManager = new PermissionManager();
		
		private final static String PERMISSION_1 = "zmiany w swoich plikach";
		
		@Test
		public void checkConnection(){
			assertNotNull(permissionManager.getConnection());
		}
		
		@Test
		public void checkAdding(){
			
			Permission permission = new Permission(PERMISSION_1);
			
			permissionManager.clearPermissions();
			assertEquals(1,permissionManager.addPermission(permission));
			
			List<Permission> permissions = permissionManager.getAllPermissions();
			Permission permissionRetrieved = permissions.get(0);
			
			assertEquals(PERMISSION_1, permissionRetrieved.getPermission());
			
		}

	

}
