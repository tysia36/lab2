package com.lab2.service;


import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.lab2.domain.Telephone;
import com.lab2.service.TelephoneManager;

public class TestingTelephone {
		
		
		TelephoneManager telManager = new TelephoneManager();
		
		private final static int TEL_1 = 502182112;
		
		@Test
		public void checkConnection(){
			assertNotNull(telManager.getConnection());
		}
		
		@Test
		public void checkAdding(){
			
			Telephone tel = new Telephone(TEL_1);
			
			telManager.clearTelephones();
			assertEquals(1,telManager.addTelephone(tel));
			
			List<Telephone> tels = telManager.getAllTelephones();
			Telephone telRetrieved = tels.get(0);
			
			assertEquals(TEL_1, telRetrieved.getTel());
			
		}

	

}

